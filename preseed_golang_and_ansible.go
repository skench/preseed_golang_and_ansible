package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

func main() {
	fmt.Println("Скачиваем пакет с github https://github.com/go-task/task/releases/download/v3.24.0/task_linux_amd64.deb")
	fmt.Println("Пакет необходим для запуска ansible-playbook.")
	fileUrl := "https://github.com/go-task/task/releases/download/v3.24.0/task_linux_amd64.deb"
	err := DownloadFile("task_linux_amd64.deb", fileUrl)
	if err != nil {
		panic(err)
	}
	fmt.Println("Downloaded: " + fileUrl)

	name, err := os.Hostname()
	if err != nil {
		panic(err)
	}
// Print hostname you PC, needed for the future, will be used to run ansible-playbook
	fmt.Println(name)
}

// DownloadFile will download a url to a local file. It's efficient because it will
// write as it downloads and not load the whole file into memory.
func DownloadFile(filepath string, url string) error {

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}
